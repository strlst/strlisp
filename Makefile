OPTIONS:=

strlisp: strlisp.c
	gcc -Wall -std=c99 -pedantic -g strlisp.c -o strlisp $(OPTIONS)

clean:
	rm -rf strlisp

run:
	./strlisp
