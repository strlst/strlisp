#include <stdio.h>
#include "./grammar.c"

#define BUFSIZE 4096
/* buffer to be used for user input */
static char buf[BUFSIZE];

int main(int argc, char** argv) {

    /* print version and exit info */
    printf("strlst lisp 0.1\n");
    //printf("<C-d> to signal EOF\n");
    printf("<C-c> to interrupt\n");

    /* lex loop */
    while (1) {
        /* prompt */
        fputs("strlisp> ", stdout);

        /* fill buffer */
        fgets(buf, BUFSIZE, stdin);

        printf("screw you for saying %s", buf);
    }

    return 0;
}
