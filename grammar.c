/* enumerator for possible parser operations */
enum g_op { or, and, many };

/* dict type that holds words */
typedef struct {
    int count;
    char* words;
    enum g_op op;
} g_dict;